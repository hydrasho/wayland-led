It's  " the xset-led on " for wayland :)

use just command : `wayland-led` or enable systemd-script 

#Installation

ArchLinux:
    `git clone https://gitlab.com/hydrasho/wayland-led`
    
    `cd wayland-led`

    `makepkg -si`

    `systemctl start wayland-led.service`

    `systemctl enable wayland-led.service`


Other:
    `git clone https://gitlab.com/hydrasho/wayland-led`
    
    `cd wayland-led`

    `meson build --prefix=/usr`

    `ninja -C build`

    `ninja -C build install`

    `systemctl start wayland-led.service`

    `systemctl enable wayland-led.service`
