//valac main.c --pkg=posix

void main(){
	string []tab_scroll = null;
	if(!is_root())
		return;
	while(true){
		Posix.sleep(5);
		start(ref tab_scroll);
	}
}

void start(ref string []tab){
	if(tab != null){
		for(var i = 0; i != tab.length;i++){
			add_lumi(tab[i]);
		}
		return;
	}
	string []tab_tmp = null;
	try{
		var directory = "/sys/class/leds/";
		var dir = Dir.open(directory, 0);
		var exp = new Regex("/sys/class/leds/input[0-9]::scrolllock");
		string name = null;
		string path = null;

		while((name = dir.read_name()) != null)
		{
			path = Path.build_filename(directory, name);

			if(exp.match(path)){
				tab_tmp += path;
				add_lumi(path);
			}
		}
	}
	catch{
		print("Erreur lecture /sys/class/leds/\n\n\n");
	}
	tab = tab_tmp;
}

bool is_root(){
	var is_root = Environment.get_user_name();
	if(is_root != "root"){
		print("\033[31m [Error]: \033[0m Please use root session\n\n\n");
		return false;
	}
	else
		return true;
}
void add_lumi(string str){
	var fd = Posix.open(str + "/brightness", Posix.O_RDWR);
	Posix.write(fd, "1", 1);
	Posix.close(fd);
}
